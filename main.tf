
resource "azurerm_resource_group" "this" {
  count    = var.resource_group_name == "" ? 1 : 0
  name     = "${local.app_service_plan_name}-rg"
  location = var.location

  tags = merge(local.default_tags, var.tags)
}
resource "azurerm_app_service_plan" "this" {

  name = local.app_service_plan_name

  location            = var.location
  resource_group_name = var.resource_group_name == "" ? azurerm_resource_group.this[0].name : var.resource_group_name
  kind                = var.kind
  reserved            = var.kind == "Linux" ? true : var.reserved

  sku {
    capacity = lookup(var.sku, "capacity", local.default_sku_capacity)
    size     = lookup(var.sku, "size", null)
    tier     = lookup(var.sku, "tier", null)
  }

  tags = merge(local.default_tags, var.tags)
}
