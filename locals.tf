locals {
  default_tags = {}

  # App Service Plan name supports only up to 60 charcters length
  app_service_plan_short = substr(var.name_prefix, 0, length(var.name_prefix) > 55 ? 55 : -1)
  app_service_plan_name  = lower("${local.app_service_plan_short}-plan")

  default_sku_capacity = var.sku["tier"] == "Dynamic" ? null : 2

}