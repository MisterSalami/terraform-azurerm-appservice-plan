
variable "location" {
  description = "Azure location in which the Plan will be created"
  type        = string
}

variable "name_prefix" {
  description = "Prefix for the app service plan name, will be suffixed, should be 55 characters maximum"
  type        = string
}

variable "resource_group_name" {
  description = "Resource Group the resources will belong to. If none provided a resource group will be created"
  type        = string
  default     = ""
}

variable "sku" {
  description = <<EOF
A Service Plan SKU block. See documentation https://www.terraform.io/docs/providers/azurerm/r/app_service_plan.html#sku.
```sku {
  tier     = "Service Plan pricing tier"
  size     = "Service Plans instance size"
  capacity = "Number of workers associated with the Service Plan, `null` in dynamic tier, otherwise defaults to 2"
  }```
EOF

  type = map(string)
}

variable "kind" {
  description = "The kind of the App Service Plan to create. See documentation https://www.terraform.io/docs/providers/azurerm/r/app_service_plan.html#kind"
  type        = string
}

variable "tags" {
  description = "Tags to add"
  type        = map(string)
  default     = {}
}

variable "reserved" {
  description = "Flag if App Service Plan should be reserved. Forced to true if \"kind\" is \"Linux\". https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/app_service_plan#reserved"
  type        = string
  default     = "false"
}
