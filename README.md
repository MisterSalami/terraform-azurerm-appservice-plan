## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 2.54.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 2.54.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_app_service_plan.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/app_service_plan) | resource |
| [azurerm_resource_group.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_kind"></a> [kind](#input\_kind) | The kind of the App Service Plan to create. See documentation https://www.terraform.io/docs/providers/azurerm/r/app_service_plan.html#kind | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | Azure location in which the Plan will be created | `string` | n/a | yes |
| <a name="input_name_prefix"></a> [name\_prefix](#input\_name\_prefix) | Prefix for the app service plan name, will be suffixed, should be 55 characters maximum | `string` | n/a | yes |
| <a name="input_reserved"></a> [reserved](#input\_reserved) | Flag if App Service Plan should be reserved. Forced to true if "kind" is "Linux". https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/app_service_plan#reserved | `string` | `"false"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Resource Group the resources will belong to. If none provided a resource group will be created | `string` | `""` | no |
| <a name="input_sku"></a> [sku](#input\_sku) | A Service Plan SKU block. See documentation https://www.terraform.io/docs/providers/azurerm/r/app_service_plan.html#sku.<pre>sku {<br>  tier     = "Service Plan pricing tier"<br>  size     = "Service Plans instance size"<br>  capacity = "Number of workers associated with the Service Plan, `null` in dynamic tier, otherwise defaults to 2"<br>  }</pre> | `map(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to add | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_app_service_plan_id"></a> [app\_service\_plan\_id](#output\_app\_service\_plan\_id) | ID of the created App Service Plan |
| <a name="output_app_service_plan_location"></a> [app\_service\_plan\_location](#output\_app\_service\_plan\_location) | Azure location of the created App Service Plan |
| <a name="output_app_service_plan_max_workers"></a> [app\_service\_plan\_max\_workers](#output\_app\_service\_plan\_max\_workers) | Maximum number of workers for the created App Service Plan |
| <a name="output_app_service_plan_name"></a> [app\_service\_plan\_name](#output\_app\_service\_plan\_name) | Name of the created App Service Plan |
