
output "app_service_plan_id" {
  description = "ID of the created App Service Plan"
  value       = azurerm_app_service_plan.this.id
}

output "app_service_plan_name" {
  description = "Name of the created App Service Plan"
  value       = azurerm_app_service_plan.this.name
}

output "app_service_plan_location" {
  description = "Azure location of the created App Service Plan"
  value       = azurerm_app_service_plan.this.location
}

output "app_service_plan_max_workers" {
  description = "Maximum number of workers for the created App Service Plan"
  value       = azurerm_app_service_plan.this.maximum_number_of_workers
}